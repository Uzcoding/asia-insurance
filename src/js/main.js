var swiper = new Swiper('.swiper--main', {
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
});

var swiper2 = new Swiper('.product__items', {
    navigation: {
        nextEl: '.product__filter-item--right',
        prevEl: '.product__filter-item--left',
    },
    slidesPerView: 1,
    simulateTouch: false,
});

$("#nav .navigation__search button").on('click', function(e) {
    e.preventDefault();
    if ($('#nav #icon').attr('src') == './img/Group 10745.svg') {
        $('#nav .navigation__search input').removeClass('hide');
        $('#nav .navigation__search input').addClass('show');
        $('#nav .navigation__search').css('position', 'absolute');
        $('#nav .navigation__search').css('right', '-25px');
        $('#nav .navigation__search').css('left', '-40px');
        $('#nav #icon').attr('src', './img/x.svg');
        $('#nav #icon').css('height', '14px');
        $('#nav #icon').css('width', '14px');
        $('#nav .navigation__list').css('opacity', '0')
        $('#nav .navigation__search').css('display', 'flex')
    } else if ($('#icon').attr('src') == './img/x.svg' && $(window).width() >= "1200") {
        $('#nav #icon').attr('src', './img/Group 10745.svg');
        $('#nav .navigation__search').css('left', '');
        $('#nav .navigation__search input').removeClass('show');
        $('#nav .navigation__search input').addClass('hide');
        $('#nav #icon').css('height', '20px');
        $('#nav #icon').css('width', '20px');
        $('#nav .navigation__search').css('right', '-25px');
        $('#nav .navigation__list').css('opacity', '1');
        $('#nav .navigation__list').css('position', 'static');
    } else if ($('#icon').attr('src') == './img/x.svg' && $(window).width() <= "1200") {
        $('#nav #icon').attr('src', './img/Group 10745.svg');
        $('#nav .navigation__search').css('left', '0px');
        $('#nav .navigation__search').css('right', '');
        $('#nav #icon').css('height', '20px');
        $('#nav #icon').css('width', '20px');
        $('#nav .navigation__list').css('opacity', '1');
        $('#nav .navigation__list').css('position', 'static');
    }
})

$(".nav-icon").on('click', function(e) {
    $("#nav .navigation__list").toggleClass('navigation__list--open');
    $('.nav-icon').toggleClass('open-x');
    if ($('.nav-icon').attr('src') == './img/Group 9187.svg') {
        $('.nav-icon').attr('src', './img/x.svg');
    } else if ($('.nav-icon').attr('src') == './img/x.svg') {
        $('.nav-icon').attr('src', './img/Group 9187.svg');
    }
});

if ($(window).width() <= '1200') {
    $('.navigation__list-item').on('click', function(e) {
        e.preventDefault();
        $(".navigation__list").removeClass('navigation__list--open');
        $('.nav-icon').attr('src', './img/Group 9187.svg');
    })
}

menu_top = $('.header__inner').offset().top; // запоминаем положение меню
$(window).scroll(function() { // отслеживаем событие прокрутки страницы
    if ($(window).scrollTop() > menu_top) { // если прокрутка дошла до меню
        if ($('.header__fixed').css('position') != 'fixed') { // проверяем, если меню еще не зафиксировано
            $('.header__fixed').css('position', 'fixed'); // задаем блоку меню свойство position = fixed
            $('.header__fixed').css('top', '0'); // положение в самом верху
            $('.header__fixed').css('left', '0'); // положение в самом верху
            $('.header__fixed').css('right', '0'); // положение в самом верху
            $('.header__fixed').css('box-shadow', '0px 0px 30px #00000012'); // тень
            $('.header').css('margin-top', '80px'); // делаем отступ, чтобы контент не "скакал" в момент фиксации меню
        }
    } else { // прокрутка страницы обратно вверх достигла место "перехода" меню
        if ($('.header__fixed').css('position') == 'fixed') { // если меню зафиксировано
            $('.header__fixed').css('box-shadow', '');
            $('.header__fixed').css('position', '');
            $('.header__fixed').css('top', '');
            $('.header__fixed').css('left', '');
            $('.header__fixed').css('right', '');
            $('.header').css('margin-top', '0');
        }
    }
});
$('a[class="navigation__list-link"]').mPageScroll2id();
new WOW().init();

if ( !$('.news__wrapper').hasClass('animated') ) {
    $('.news__wrapper').addClass('news__wrapper--hov')
} 


